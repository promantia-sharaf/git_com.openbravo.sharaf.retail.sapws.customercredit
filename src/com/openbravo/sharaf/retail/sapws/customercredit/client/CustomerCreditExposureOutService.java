
package com.openbravo.sharaf.retail.sapws.customercredit.client;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "CustomerCreditExposure_OutService", targetNamespace = "http://sharafdg.com/OB/CustomerCreditExposure", wsdlLocation = "wsdl/CustomerCredit.wsdl.xml")
public class CustomerCreditExposureOutService
    extends Service
{

    private final static URL CUSTOMERCREDITEXPOSUREOUTSERVICE_WSDL_LOCATION;
    private final static WebServiceException CUSTOMERCREDITEXPOSUREOUTSERVICE_EXCEPTION;
    private final static QName CUSTOMERCREDITEXPOSUREOUTSERVICE_QNAME = new QName("http://sharafdg.com/OB/CustomerCreditExposure", "CustomerCreditExposure_OutService");

    static {
        CUSTOMERCREDITEXPOSUREOUTSERVICE_WSDL_LOCATION = com.openbravo.sharaf.retail.sapws.customercredit.client.CustomerCreditExposureOutService.class.getResource("wsdl/CustomerCredit.wsdl.xml");
        WebServiceException e = null;
        if (CUSTOMERCREDITEXPOSUREOUTSERVICE_WSDL_LOCATION == null) {
            e = new WebServiceException("Cannot find 'wsdl/CustomerCredit.wsdl.xml' wsdl. Place the resource correctly in the classpath.");
        }
        CUSTOMERCREDITEXPOSUREOUTSERVICE_EXCEPTION = e;
    }

    public CustomerCreditExposureOutService() {
        super(__getWsdlLocation(), CUSTOMERCREDITEXPOSUREOUTSERVICE_QNAME);
    }

    public CustomerCreditExposureOutService(WebServiceFeature... features) {
        super(__getWsdlLocation(), CUSTOMERCREDITEXPOSUREOUTSERVICE_QNAME, features);
    }

    public CustomerCreditExposureOutService(URL wsdlLocation) {
        super(wsdlLocation, CUSTOMERCREDITEXPOSUREOUTSERVICE_QNAME);
    }

    public CustomerCreditExposureOutService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, CUSTOMERCREDITEXPOSUREOUTSERVICE_QNAME, features);
    }

    public CustomerCreditExposureOutService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public CustomerCreditExposureOutService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns CustomerCreditExposureOut
     */
    @WebEndpoint(name = "HTTP_Port")
    public CustomerCreditExposureOut getHTTPPort() {
        return super.getPort(new QName("http://sharafdg.com/OB/CustomerCreditExposure", "HTTP_Port"), CustomerCreditExposureOut.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns CustomerCreditExposureOut
     */
    @WebEndpoint(name = "HTTP_Port")
    public CustomerCreditExposureOut getHTTPPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://sharafdg.com/OB/CustomerCreditExposure", "HTTP_Port"), CustomerCreditExposureOut.class, features);
    }

    /**
     * 
     * @return
     *     returns CustomerCreditExposureOut
     */
    @WebEndpoint(name = "HTTPS_Port")
    public CustomerCreditExposureOut getHTTPSPort() {
        return super.getPort(new QName("http://sharafdg.com/OB/CustomerCreditExposure", "HTTPS_Port"), CustomerCreditExposureOut.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns CustomerCreditExposureOut
     */
    @WebEndpoint(name = "HTTPS_Port")
    public CustomerCreditExposureOut getHTTPSPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://sharafdg.com/OB/CustomerCreditExposure", "HTTPS_Port"), CustomerCreditExposureOut.class, features);
    }

    private static URL __getWsdlLocation() {
        if (CUSTOMERCREDITEXPOSUREOUTSERVICE_EXCEPTION!= null) {
            throw CUSTOMERCREDITEXPOSUREOUTSERVICE_EXCEPTION;
        }
        return CUSTOMERCREDITEXPOSUREOUTSERVICE_WSDL_LOCATION;
    }

}
