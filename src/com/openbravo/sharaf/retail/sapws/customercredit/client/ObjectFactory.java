
package com.openbravo.sharaf.retail.sapws.customercredit.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.openbravo.sharaf.retail.sapws.customercredit.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CustomerCreditExposureRequest_QNAME = new QName("http://sharafdg.com/OB/CustomerCreditExposure", "CustomerCreditExposureRequest");
    private final static QName _CustomerCreditExposureResponse_QNAME = new QName("http://sharafdg.com/OB/CustomerCreditExposure", "CustomerCreditExposureResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.openbravo.sharaf.retail.sapws.customercredit.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CustomerCreditExposureResponse }
     * 
     */
    public CustomerCreditExposureResponse createCustomerCreditExposureResponse() {
        return new CustomerCreditExposureResponse();
    }

    /**
     * Create an instance of {@link CustomerCreditExposureRequest }
     * 
     */
    public CustomerCreditExposureRequest createCustomerCreditExposureRequest() {
        return new CustomerCreditExposureRequest();
    }

    /**
     * Create an instance of {@link CustomerCreditExposureResponse.CCEResponse }
     * 
     */
    public CustomerCreditExposureResponse.CCEResponse createCustomerCreditExposureResponseCCEResponse() {
        return new CustomerCreditExposureResponse.CCEResponse();
    }

    /**
     * Create an instance of {@link CustomerCreditExposureRequest.CCERequest }
     * 
     */
    public CustomerCreditExposureRequest.CCERequest createCustomerCreditExposureRequestCCERequest() {
        return new CustomerCreditExposureRequest.CCERequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerCreditExposureRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sharafdg.com/OB/CustomerCreditExposure", name = "CustomerCreditExposureRequest")
    public JAXBElement<CustomerCreditExposureRequest> createCustomerCreditExposureRequest(CustomerCreditExposureRequest value) {
        return new JAXBElement<CustomerCreditExposureRequest>(_CustomerCreditExposureRequest_QNAME, CustomerCreditExposureRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerCreditExposureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sharafdg.com/OB/CustomerCreditExposure", name = "CustomerCreditExposureResponse")
    public JAXBElement<CustomerCreditExposureResponse> createCustomerCreditExposureResponse(CustomerCreditExposureResponse value) {
        return new JAXBElement<CustomerCreditExposureResponse>(_CustomerCreditExposureResponse_QNAME, CustomerCreditExposureResponse.class, null, value);
    }

}
