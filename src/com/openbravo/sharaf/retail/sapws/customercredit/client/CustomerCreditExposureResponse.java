
package com.openbravo.sharaf.retail.sapws.customercredit.client;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerCreditExposureResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerCreditExposureResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CCEResponse" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustomerNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CreditControllingArea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CustomerCrediExposure" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="TotalCrediExposure" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="RemainingCredit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerCreditExposureResponse", propOrder = {
    "cceResponse"
})
public class CustomerCreditExposureResponse {

    @XmlElement(name = "CCEResponse")
    protected List<CustomerCreditExposureResponse.CCEResponse> cceResponse;

    /**
     * Gets the value of the cceResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cceResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCCEResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomerCreditExposureResponse.CCEResponse }
     * 
     * 
     */
    public List<CustomerCreditExposureResponse.CCEResponse> getCCEResponse() {
        if (cceResponse == null) {
            cceResponse = new ArrayList<CustomerCreditExposureResponse.CCEResponse>();
        }
        return this.cceResponse;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustomerNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CreditControllingArea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CustomerCrediExposure" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="TotalCrediExposure" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="RemainingCredit" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customerNo",
        "creditControllingArea",
        "customerCrediExposure",
        "totalCrediExposure",
        "remainingCredit",
        "currency",
        "status"
    })
    public static class CCEResponse {

        @XmlElement(name = "CustomerNo")
        protected String customerNo;
        @XmlElement(name = "CreditControllingArea")
        protected String creditControllingArea;
        @XmlElement(name = "CustomerCrediExposure")
        protected BigDecimal customerCrediExposure;
        @XmlElement(name = "TotalCrediExposure")
        protected BigDecimal totalCrediExposure;
        @XmlElement(name = "RemainingCredit")
        protected BigDecimal remainingCredit;
        @XmlElement(name = "Currency")
        protected String currency;
        @XmlElement(name = "Status")
        protected String status;

        /**
         * Gets the value of the customerNo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerNo() {
            return customerNo;
        }

        /**
         * Sets the value of the customerNo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerNo(String value) {
            this.customerNo = value;
        }

        /**
         * Gets the value of the creditControllingArea property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreditControllingArea() {
            return creditControllingArea;
        }

        /**
         * Sets the value of the creditControllingArea property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreditControllingArea(String value) {
            this.creditControllingArea = value;
        }

        /**
         * Gets the value of the customerCrediExposure property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getCustomerCrediExposure() {
            return customerCrediExposure;
        }

        /**
         * Sets the value of the customerCrediExposure property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setCustomerCrediExposure(BigDecimal value) {
            this.customerCrediExposure = value;
        }

        /**
         * Gets the value of the totalCrediExposure property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTotalCrediExposure() {
            return totalCrediExposure;
        }

        /**
         * Sets the value of the totalCrediExposure property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTotalCrediExposure(BigDecimal value) {
            this.totalCrediExposure = value;
        }

        /**
         * Gets the value of the remainingCredit property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getRemainingCredit() {
            return remainingCredit;
        }

        /**
         * Sets the value of the remainingCredit property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setRemainingCredit(BigDecimal value) {
            this.remainingCredit = value;
        }

        /**
         * Gets the value of the currency property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrency() {
            return currency;
        }

        /**
         * Sets the value of the currency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrency(String value) {
            this.currency = value;
        }

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

    }

}
