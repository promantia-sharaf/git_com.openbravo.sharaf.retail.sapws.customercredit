/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.sapws.customercredit.processes;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.MobileService.MobileServiceQualifier;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.retail.posterminal.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openbravo.sharaf.retail.sapws.customercredit.CustomerCreditSAPService;
import com.openbravo.sharaf.retail.sapws.customercredit.SAPCC_Config;

@MobileServiceQualifier(serviceName = "org.openbravo.retail.posterminal.CheckBusinessPartnerCredit")
public class CheckBusinessPartnerCredit extends JSONProcessSimple {
  private static final Logger log = LoggerFactory.getLogger(CheckBusinessPartnerCredit.class);

  @Inject
  private CustomerCreditSAPService ccService;

  @Override
  public JSONObject exec(JSONObject jsonsent) throws OBException, JSONException, ServletException {
    try {
      log.debug("Start CheckBusinessPartnerCredit request: {}", jsonsent.toString(2));
    } catch (JSONException ignore) {
    }
    JSONObject respObject = new JSONObject();
    try {
      OBContext.setAdminMode(true);

      String businessPartnerId = jsonsent.getString("businessPartnerId");
      Double doubleTotalPending = jsonsent.getDouble("totalPending");
      BigDecimal totalPending = new BigDecimal(doubleTotalPending);
      BusinessPartner businessPartner = OBDal.getInstance().get(BusinessPartner.class,
          businessPartnerId);

      String creditControllingArea = null;
      // Read service configuration
      OBCriteria<SAPCC_Config> criteria = OBDal.getInstance().createCriteria(SAPCC_Config.class);
      criteria.addOrderBy(SAPCC_Config.PROPERTY_LINENO, true);
      criteria.setMaxResults(1);
      SAPCC_Config config = (SAPCC_Config) criteria.uniqueResult();
      if (config.getCreditcontrollingarea() != null) {
        creditControllingArea = config.getCreditcontrollingarea();
      } else {
        creditControllingArea = "Z105";
      }
      BigDecimal actualCredit = ((CustomerCreditSAPService) ccService).getRemainingCredit(
          businessPartner.getSearchKey(), creditControllingArea);

      boolean enoughCredit = false;

      if (actualCredit != null) {
        if (actualCredit.compareTo(totalPending) == -1) {
          enoughCredit = false;
        } else {
          enoughCredit = true;
        }
      }
      respObject.put("enoughCredit", enoughCredit);
      respObject.put("actualCredit", actualCredit);
      respObject.put("bpName", businessPartner.getName());
    } catch (Exception e) {
      throw new OBException(e.getMessage(), e);
    } finally {
      OBContext.restorePreviousMode();
    }
    JSONObject result = new JSONObject();
    result.put(JsonConstants.RESPONSE_DATA, respObject);
    result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);

    return result;

  }

  @Override
  protected int getPriority() {
    return 99;
  }

  @Override
  protected String getProperty() {
    return "OBPOS_receipt.creditsales";
  }

}
