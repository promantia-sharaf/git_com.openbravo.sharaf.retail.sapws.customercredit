/*
 ************************************************************************************
 * Copyright (C) 2018-2020 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.sapws.customercredit;

import java.math.BigDecimal;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.servlet.ServletException;
import javax.xml.ws.BindingProvider;

import org.apache.commons.lang.StringUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.utils.FormatUtilities;

import com.openbravo.sharaf.retail.sapws.customercredit.client.CustomerCreditExposureOut;
import com.openbravo.sharaf.retail.sapws.customercredit.client.CustomerCreditExposureOutService;
import com.openbravo.sharaf.retail.sapws.customercredit.client.CustomerCreditExposureRequest;
import com.openbravo.sharaf.retail.sapws.customercredit.client.CustomerCreditExposureResponse;

/**
 * Utility class to get customer credit from SAP web service.
 * 
 * @author rctoledano
 */

@ApplicationScoped
public class CustomerCreditSAPService {

  private String user;
  private String passwd;
  private String endpoint;

  private CustomerCreditExposureOutService ccService;
  private CustomerCreditExposureOut ccPort;

  public CustomerCreditSAPService() throws ServletException {
    this.initialize();
  }

  private void initialize() throws ServletException {
    user = null;
    passwd = null;
    endpoint = null;

    OBContext.setAdminMode(false);
    try {
      // Read service configuration
      OBCriteria<SAPCC_Config> criteria = OBDal.getInstance().createCriteria(SAPCC_Config.class);
      criteria.addOrderBy(SAPCC_Config.PROPERTY_LINENO, true);
      criteria.setMaxResults(1);
      SAPCC_Config config = (SAPCC_Config) criteria.uniqueResult();
      if (config != null) {
        user = config.getUsername();
        endpoint = config.getEndpoint();
        String password = config.getPassword();
        if (!StringUtils.isEmpty(password)) {
          passwd = FormatUtilities.encryptDecrypt(password, false);
        }
      }

      ccService = new CustomerCreditExposureOutService();
      ccPort = ccService.getHTTPPort();
      BindingProvider bp = (BindingProvider) ccPort;

      // Set new endpoint
      if (!StringUtils.isEmpty(endpoint)) {
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
      }

      // Set Basic HTTP Authentication
      if (!StringUtils.isEmpty(user) && !StringUtils.isEmpty(passwd)) {
        bp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, user);
        bp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, passwd);
      }
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  public BigDecimal getRemainingCredit(final String customerNo,
      final String creditControllingArea) {

    // Create the request
    CustomerCreditExposureRequest ccRequest = new CustomerCreditExposureRequest();
    CustomerCreditExposureRequest.CCERequest ccParam = new CustomerCreditExposureRequest.CCERequest();
    ccParam.setCustomerNo(customerNo);
    if (!StringUtils.isEmpty(creditControllingArea)) {
      ccParam.setCreditControllingArea(creditControllingArea);
    }
    ccRequest.getCCERequest().add(ccParam);

    // Consume service method
    CustomerCreditExposureResponse ccResponse = ccPort.customerCreditExposureOut(ccRequest);
    BigDecimal remainingCredit = null;
    if (ccResponse != null) {
      List<CustomerCreditExposureResponse.CCEResponse> ccList = ccResponse.getCCEResponse();
      if (!ccList.isEmpty() && ccList.get(0).getStatus().compareTo("Success") == 0) {
        remainingCredit = ccList.get(0).getRemainingCredit();
      }
    }

    return remainingCredit;
  }
}
